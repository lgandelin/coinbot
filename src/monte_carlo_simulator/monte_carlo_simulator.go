package monte_carlo_simulator

import (
	. "gitlab.com/lgandelin/common"
	"math"
	"math/rand"
	"sort"
	"time"
)

func GenerateTradesDistributions(trades TradesList, simulations int, years float64) []TradesList {

	//Sort trades by date
	sort.Sort(trades)

	trading_years := trades[len(trades)-1].CloseTime.Sub(trades[0].CloseTime).Hours() / 24 / 365

	//Separate trades by system
	systemsTrades := groupTradesBySystems(trades)

	trades_distributions := []TradesList{}

	for i := 0; i < simulations; i++ {

		trades := TradesList{}
		for _, systemTrades := range systemsTrades {
			sample_size := int(math.Ceil((years / trading_years) * float64(len(systemTrades))))
			trades = append(trades, getShuffledTrades(systemTrades, sample_size)...)
		}

		trades_distributions = append(trades_distributions, trades)
	}

	return trades_distributions
}

func groupTradesBySystems(trades TradesList) map[string]TradesList {
	systemsTrades := make(map[string]TradesList)

	for _, trade := range trades {
		systemsTrades[trade.SystemID] = append(systemsTrades[trade.SystemID], trade)
	}

	return systemsTrades
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}

	return false
}

func getShuffledTrades(trades TradesList, number int) TradesList {
	rand.Seed(time.Now().UTC().UnixNano())
	new_trades := TradesList{}

	for i := 0; i < number; i++ {
		j := rand.Intn(len(trades))
		new_trades = append(new_trades, trades[j])
	}

	return new_trades
}
