package analyzer

import (
	"fmt"
	. "gitlab.com/lgandelin/common"
	"math"
	"sort"

	"github.com/montanaflynn/stats"
)

func WinPercentage(trades TradesList) float64 {
	winning_trades_number := 0

	if total_trades_number := len(trades); total_trades_number > 0 {
		for _, trade := range trades {
			if trade.Operation == "BUY" && trade.ClosePrice-trade.OpenPrice > 0 ||
				trade.Operation == "SELL" && trade.OpenPrice-trade.ClosePrice > 0 {
				winning_trades_number++
			}
		}

		return float64(winning_trades_number) / float64(total_trades_number)
	}

	return 0
}

func LossPercentage(trades TradesList) float64 {
	losing_trades_number := 0

	if total_trades_number := len(trades); total_trades_number > 0 {
		for _, trade := range trades {
			if trade.Operation == "BUY" && trade.ClosePrice-trade.OpenPrice <= 0 ||
				trade.Operation == "SELL" && trade.OpenPrice-trade.ClosePrice <= 0 {
				losing_trades_number++
			}
		}

		return float64(losing_trades_number) / float64(total_trades_number)
	}

	return 0
}

func AverageWinningTradeInRMultiple(trades TradesList) float64 {
	winnersRMultiple := []float64{}
	for _, trade := range trades {
		if trade.Delta() > 0 {
			winnersRMultiple = append(winnersRMultiple, trade.RMultiple())
		}
	}
	average, _ := stats.Mean(winnersRMultiple)

	return average
}

func AverageLosingTradeInRMultiple(trades TradesList) float64 {
	losersRMultiple := []float64{}
	for _, trade := range trades {
		if trade.Delta() <= 0 {
			losersRMultiple = append(losersRMultiple, trade.RMultiple())
		}
	}
	average, _ := stats.Mean(losersRMultiple)

	return average
}

func AverageTradeInRMultiple(trades TradesList) float64 {
	tradesRMultiple := []float64{}
	for _, trade := range trades {
		tradesRMultiple = append(tradesRMultiple, trade.RMultiple())
	}
	average, _ := stats.Mean(tradesRMultiple)

	return average
}

func ExpectancyInRMultiple(trades TradesList) float64 {
	return AverageWinningTradeInRMultiple(trades)*WinPercentage(trades) + AverageLosingTradeInRMultiple(trades)*LossPercentage(trades)
}

func TharpExpectancyInRMultiple(trades TradesList) float64 {
	return ExpectancyInRMultiple(trades) / -AverageLosingTradeInRMultiple(trades)
}

func standardDeviationInRMultiple(trades TradesList) float64 {
	values := []float64{}
	for _, trade := range trades {
		values = append(values, trade.RMultiple())
	}
	stdev, _ := stats.StandardDeviationPopulation(values)
	return stdev
}

func meanInRMultiple(trades TradesList) float64 {
	values := []float64{}
	for _, trade := range trades {
		values = append(values, trade.RMultiple())
	}
	mean, _ := stats.Mean(values)
	return mean
}

func TotalRMultiple(trades TradesList) float64 {
	total := 0.0
	for _, trade := range trades {
		total += trade.RMultiple()
	}
	return total
}

func extractEquityValues(equity EquityCurve) []float64 {
	equityValues := []float64{}
	for _, e := range equity {
		equityValues = append(equityValues, e.Value)
	}

	return equityValues
}

func maxDrawdown(equity EquityCurve) float64 {
	maxDrawdown := 0.0
	equityValues := extractEquityValues(equity)

	for i, point := range equityValues {
		followingEquity := equityValues[i : len(equityValues)-1]
		for _, otherPoint := range followingEquity {
			if newDrawdown := (otherPoint / point) - 1; newDrawdown < maxDrawdown {
				maxDrawdown = newDrawdown
			}
		}
	}

	if maxDrawdown < -99.99 {
		maxDrawdown = -99.99
	}

	return -maxDrawdown
}

func CAR(equity EquityCurve, years float64) float64 {
	equityValues := extractEquityValues(equity)

	return math.Pow(equityValues[len(equityValues)-1]/equityValues[0], (1/years)) - 1
}

func RAR(equity EquityCurve, years float64) float64 {
	equityValues := extractEquityValues(equity)

	serie := []stats.Coordinate{}
	for i, value := range equityValues {
		serie = append(serie, stats.Coordinate{float64(i), value})
	}
	r, _ := stats.LinearRegression(serie)

	first_rar_equity_value := r[0].Y
	last_rar_equity_value := r[len(serie)-1].Y

	return math.Pow(last_rar_equity_value/first_rar_equity_value, (1/years)) - 1
}

func PrintMonteCarloReport(trades_distributions []TradesList, equity_curves []EquityCurve, years float64) {

	fmt.Println("------------------------------------")

	max_drawdowns := []float64{}
	cars := []float64{}
	rars := []float64{}
	final_equities := []float64{}

	for _, equity_curve := range equity_curves {
		max_drawdowns = append(max_drawdowns, maxDrawdown(equity_curve))
		cars = append(cars, CAR(equity_curve, years))
		rars = append(rars, RAR(equity_curve, years))
		final_equities = append(final_equities, equity_curve[len(equity_curve)-1].Value)
	}

	//Max drawdowns
	sort.Float64s(max_drawdowns)
	dd95, _ := stats.Percentile(max_drawdowns, 95.0)
	max_dd_median, _ := stats.Median(max_drawdowns)

	//Cars
	sort.Float64s(cars)
	car25, _ := stats.Percentile(cars, 25.0)
	car_median, _ := stats.Median(cars)

	//Cars
	sort.Float64s(rars)
	rar25, _ := stats.Percentile(rars, 25.0)
	rar_median, _ := stats.Median(rars)

	//Final equities
	sort.Float64s(final_equities)
	final_equity25, _ := stats.Percentile(final_equities, 25.0)
	final_equity_median, _ := stats.Median(final_equities)

	fmt.Printf("- %%95 Max DD : %.2f%%\n", dd95*100)
	fmt.Printf("- Median max DD : %.2f%%\n", max_dd_median*100)

	fmt.Println("------------------------------------")

	fmt.Printf("- %%25 CAR : %.2f%%\n", car25*100)
	fmt.Printf("- Median CAR : %.2f%%\n", car_median*100)

	fmt.Println("------------------------------------")

	fmt.Printf("- %%25 RAR : %.2f%%\n", rar25*100)
	fmt.Printf("- Median RAR : %.2f%%\n", rar_median*100)

	fmt.Println("------------------------------------")

	fmt.Printf("- Median RAR / Median max DD : %.2f\n", rar_median/max_dd_median)

	fmt.Println("------------------------------------")
	fmt.Printf("- %%25 final equity : %6.2fBTC\n", final_equity25)
	fmt.Printf("- Median final equity : %6.2fBTC\n", final_equity_median)

	fmt.Println("------------------------------------")
}
