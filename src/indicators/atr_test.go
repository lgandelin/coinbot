package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
)

func TestTR(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		Time:                ParseTime("2017-07-01 16:00:00"),
		CandlesticksGateway: NewMockCandlesticksGateway(),
	}
	assert.InDelta(t, 0.00007930, context.TR(0), 0.00000001)

	context.Time = ParseTime("2017-07-02 11:00:00")
	assert.InDelta(t, 0.00010515, context.TR(0), 0.00000001)
	assert.InDelta(t, 0.00009653, context.TR(5), 0.00000001)
}

func TestATR(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		Time:                ParseTime("2017-07-02 11:00:00"),
		CandlesticksGateway: NewMockCandlesticksGateway(),
	}
	assert.InDelta(t, 0.00011555, context.ATR(14, 0), 0.00000001)
	assert.InDelta(t, 0.00012160, context.ATR(14, 12), 0.00000001)
}
