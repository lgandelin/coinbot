package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
)

func TestMA(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		Time:                ParseTime("2017-07-01 16:00:00"),
		CandlesticksGateway: NewMockCandlesticksGateway(),
	}
	assert.InDelta(t, 0.01581607, context.SMA(14, 0), 0.00000001)

	context.Time = ParseTime("2017-07-02 02:00:00")
	assert.InDelta(t, 0.0156078829, context.SMA(14, 0), 0.00000001)

	context.Time = ParseTime("2017-07-02 11:00:00")
	assert.InDelta(t, 0.0158169129, context.SMA(14, 0), 0.00000001)
	assert.InDelta(t, 0.0156969614, context.SMA(14, 5), 0.00000001)
}
