package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
)

func TestOpen(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		Time:                ParseTime("2017-07-02 11:00:00"),
		CandlesticksGateway: NewMockCandlesticksGateway(),
	}
	assert.InDelta(t, 0.0158541, context.Open(0), 0.00000001)
}
