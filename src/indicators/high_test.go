package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
)

func TestHigh(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		Time:                ParseTime("2017-07-02 11:00:00"),
		CandlesticksGateway: NewMockCandlesticksGateway(),
	}
	assert.InDelta(t, 0.01594550, context.High(0), 0.00000001)
}
