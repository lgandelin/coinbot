package common

import (
	"time"
)

type MemoryTradesGateway struct {
	Trades TradesList
}

func (gateway *MemoryTradesGateway) OpenTrade(trade *Trade) {
	gateway.Trades = append(gateway.Trades, trade)
}

func (gateway *MemoryTradesGateway) UpdateTradeSetHasChild(trade *Trade) {
	trade.HasChildTrade = true
}

func (gateway *MemoryTradesGateway) CloseTrade(trade *Trade, time time.Time, close_price float64) {
	trade.CloseTime = time
	trade.ClosePrice = close_price
}

func (gateway MemoryTradesGateway) GetOpenedTrades() TradesList {
	trades := TradesList{}

	for _, trade := range gateway.Trades {
		if trade.IsOpened() {
			trades = append(trades, trade)
		}
	}

	return trades
}

func (gateway MemoryTradesGateway) GetTrades() TradesList {
	return gateway.Trades
}
