package common

import (
	"time"
)

type Trade struct {
	ID            string
	Operation     string
	Currency      string
	OpenPrice     float64
	OpenTime      time.Time
	ClosePrice    float64
	CloseTime     time.Time
	StopLossPrice float64
	Position      float64
	SystemID      string
	HasChildTrade bool
}

func (trade *Trade) StopLoss() float64 {
	pips := 0.0
	if trade.Operation == "BUY" {
		pips = trade.OpenPrice - trade.StopLossPrice
	} else if trade.Operation == "SELL" {
		pips = trade.StopLossPrice - trade.OpenPrice
	}

	return pips
}

func (trade *Trade) Delta() float64 {
	pips := 0.0
	if trade.Operation == "BUY" {
		pips = trade.ClosePrice - trade.OpenPrice
	} else if trade.Operation == "SELL" {
		pips = trade.OpenPrice - trade.ClosePrice
	}

	return pips
}

func (trade Trade) Amount() float64 {
	spread_and_slippage := 0.005
	amount := (trade.Delta() - spread_and_slippage*trade.Delta()) * trade.Position

	return amount
}

func (trade *Trade) RMultiple() float64 {
	return trade.Delta() / trade.StopLoss()
}

func (trade *Trade) IsOpened() bool {
	return trade.CloseTime.IsZero()
}
