package common

type TradesManager struct {
	Context Context
}

func (trades_manager *TradesManager) ManageTrades(system func(Context) System) {
	context := trades_manager.Context

	s := system(context)

	if context.Logger != nil {
		context.Logger.Info("Checking trades", LoggerFields{
			"currency":  context.Currency,
			"system":    s.ID,
			"time":      context.Time,
			"timeframe": context.Timeframe,
		})
	}

	for _, trade := range context.TradesGateway.GetOpenedTrades() {

		//Stop loss
		if trades_manager.IsStopLossTriggered(trade) {
			context.TradesGateway.CloseTrade(trade, context.Time, trade.StopLossPrice)

			if context.Logger != nil {
				context.Logger.Info("Stop loss triggered", LoggerFields{
					"currency":        context.Currency,
					"system":          s.ID,
					"time":            context.Time,
					"timeframe":       context.Timeframe,
					"trade":           trade.ID,
					"stop_loss_price": trade.StopLossPrice,
				})
			}
			break
		}

		//Standard exit
		if s.Exit() {
			context.TradesGateway.CloseTrade(trade, context.Time, context.Close(0))

			if context.Logger != nil {
				context.Logger.Info("Standard exit triggered", LoggerFields{
					"currency":    context.Currency,
					"system":      s.ID,
					"time":        context.Time,
					"timeframe":   context.Timeframe,
					"trade":       trade,
					"close_price": context.Close(0),
				})
			}
		}

		//Pyramid entry
		if s.Operation == "BUY" && !trade.HasChildTrade && s.ChildTradesThreshold() > 0.0 {
			if context.Close(0)-trade.OpenPrice > s.ChildTradesThreshold() && s.ChildTradesEntry() {
				child_trade := Trade{
					Operation: s.Operation,
					Currency:  context.Currency,
					OpenTime:  context.Time,
					OpenPrice: context.Close(0),
					SystemID:  s.ID,
				}

				//Set stop loss
				if s.StopLoss() != 0.0 {
					if child_trade.Operation == "BUY" {
						child_trade.StopLossPrice = RoundFloat(child_trade.OpenPrice-s.StopLoss(), 8)
					}

					if child_trade.Operation == "SELL" {
						child_trade.StopLossPrice = RoundFloat(child_trade.OpenPrice+s.StopLoss(), 8)
					}
				}

				context.TradesGateway.OpenTrade(&child_trade)

				context.TradesGateway.UpdateTradeSetHasChild(trade)

				if context.Logger != nil {
					context.Logger.Info("Child trade entry triggered", LoggerFields{
						"currency":    context.Currency,
						"system":      s.ID,
						"time":        context.Time,
						"timeframe":   context.Timeframe,
						"trade":       trade,
						"child_trade": trade,
					})
				}
			}
		}
	}

	//Standard entry
	if s.Entry() {

		trade := Trade{
			Operation: s.Operation,
			Currency:  context.Currency,
			OpenTime:  context.Time,
			OpenPrice: context.Close(0),
			SystemID:  s.ID,
		}

		//Set stop loss
		if s.StopLoss() != 0.0 {
			if trade.Operation == "BUY" {
				trade.StopLossPrice = RoundFloat(trade.OpenPrice-s.StopLoss(), 8)
			}

			if trade.Operation == "SELL" {
				trade.StopLossPrice = RoundFloat(trade.OpenPrice+s.StopLoss(), 8)
			}
		}

		trade.Position = Position(context.EquityGateway.GetCurrentEquity(), GetEquityPercent(), trade.StopLoss())

		context.TradesGateway.OpenTrade(&trade)

		if context.Logger != nil {
			context.Logger.Info("Standard entry triggered", LoggerFields{
				"currency":  context.Currency,
				"system":    s.ID,
				"time":      context.Time,
				"timeframe": context.Timeframe,
				"trade":     trade,
			})
		}
	}
}

func (trades_manager *TradesManager) IsStopLossTriggered(trade *Trade) bool {
	if trade.StopLossPrice > 0.0 {
		if trade.Operation == "BUY" && (trades_manager.Context.Close(0) < trade.StopLossPrice) ||
			(trade.Operation == "SELL" && (trades_manager.Context.Close(0) > trade.StopLossPrice)) {
			return true
		}
	}

	return false
}
