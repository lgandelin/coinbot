package common

import (
	"time"
)

type EquityGateway interface {
	GetCurrentEquity() float64
	GetEquityCurve() EquityCurve
	UpdateCurrentEquity(value float64, t time.Time)
}
