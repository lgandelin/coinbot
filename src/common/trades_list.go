package common

type TradesList []*Trade

func (trades_list TradesList) Len() int {
	return len(trades_list)
}

func (trades_list TradesList) Less(i, j int) bool {
	return trades_list[i].OpenTime.Before(trades_list[j].OpenTime)
}

func (trades_list TradesList) Swap(i, j int) {
	trades_list[i], trades_list[j] = trades_list[j], trades_list[i]
}
