package common

import (
	"time"
)

type Candlestick struct {
	Currency  string
	Timeframe string
	CloseTime time.Time
	Open      float64
	High      float64
	Low       float64
	Close     float64
}
