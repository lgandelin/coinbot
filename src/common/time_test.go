package common

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseTime(t *testing.T) {
	date_time := ParseTime("2017-07-02 17:38:05")

	assert.Equal(t, 2017, date_time.Year())
	assert.Equal(t, time.Month(7), date_time.Month())
	assert.Equal(t, 02, date_time.Day())
	assert.Equal(t, 17, date_time.Hour())
	assert.Equal(t, 38, date_time.Minute())
	assert.Equal(t, 05, date_time.Second())
}

func TestShiftTime(t *testing.T) {
	assert.Equal(t, ParseTime("2017-07-02 17:30:00"), ShiftTime(ParseTime("2017-07-02 17:30:00"), "M30", 0))
	assert.Equal(t, ParseTime("2017-07-02 17:00:00"), ShiftTime(ParseTime("2017-07-02 17:30:00"), "M30", 1))
}

func TestRoundTime(t *testing.T) {
	assert.Equal(t, ParseTime("2017-07-02 17:00:00"), RoundTime(ParseTime("2017-07-02 17:00:00"), "M30"))
	assert.Equal(t, ParseTime("2017-07-02 17:30:00"), RoundTime(ParseTime("2017-07-02 17:30:01"), "M30"))
	assert.Equal(t, ParseTime("2017-07-02 17:30:00"), RoundTime(ParseTime("2017-07-02 17:36:45"), "M30"))
}
