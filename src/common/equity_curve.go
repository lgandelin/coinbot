package common

import (
	"time"
)

type EquityCurve []Equity

func (equity_curve EquityCurve) LastValue() float64 {
	return equity_curve[len(equity_curve)-1].Value
}

type Equity struct {
	Time  time.Time
	Value float64
}

type EquityCurves []EquityCurve

func (equity_curves EquityCurves) Len() int {
	return len(equity_curves)
}

func (equity_curves EquityCurves) Less(i, j int) bool {
	return equity_curves[i].LastValue() < equity_curves[j].LastValue()
}

func (equity_curves EquityCurves) Swap(i, j int) {
	equity_curves[i], equity_curves[j] = equity_curves[j], equity_curves[i]
}
