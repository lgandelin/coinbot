package common

type Logger interface {
	Info(message string, fields LoggerFields)
}

type LoggerFields map[string]interface{}
