package common

func GetEquityPercent() float64 {
	equity_percent := 2.0

	return equity_percent
}

func GenerateEquityCurves(context Context, trades_distributions []TradesList, starting_equity float64) []EquityCurve {
	equity_curves := []EquityCurve{}

	for _, trades := range trades_distributions {
		equity_gateway := MemoryEquityGateway{}
		equity_gateway.UpdateCurrentEquity(starting_equity, context.Time)

		for _, trade := range trades {
			trade.Position = Position(equity_gateway.GetCurrentEquity(), GetEquityPercent(), trade.StopLoss())
			equity_gateway.UpdateCurrentEquity(equity_gateway.GetCurrentEquity()+trade.Amount(), trade.CloseTime)
		}
		equity_curves = append(equity_curves, equity_gateway.GetEquityCurve())
	}

	return equity_curves
}

func Position(equity float64, equity_risk_in_percent float64, stop_loss float64) float64 {
	return equity * (equity_risk_in_percent / 100) / stop_loss
}
