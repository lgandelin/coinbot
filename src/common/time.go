package common

import (
	"log"
	"time"
)

func ParseTime(s string) time.Time {
	t, error := time.Parse("2006-01-02 15:04:05", s)

	if error != nil {
		log.Fatal(error)
	}

	return t
}

func ShiftTime(t time.Time, timeframe string, shift int, forward ...bool) time.Time {
	move := -1
	if len(forward) > 0 {
		move = 1
	}

	for i := 1; i <= shift; i++ {
		if timeframe == "M30" {
			t = t.Add(time.Duration(move*30) * time.Minute)
		}
	}

	return t
}

func RoundTime(t time.Time, timeframe string) time.Time {

	for t.Second() != 0 {
		t = t.Add(time.Duration(-1) * time.Second)
	}

	if timeframe == "M30" {
		for t.Minute() != 0 && t.Minute() != 30 {
			t = t.Add(time.Duration(-1) * time.Minute)
		}
	}

	return t
}
