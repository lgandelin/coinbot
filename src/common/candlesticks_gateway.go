package common

import (
	"time"
)

type CandlesticksGateway interface {
	GetCandlestick(currency string, timeframe string, time time.Time) Candlestick
	GetCandlesticks(currency string, timeframe string, start_time time.Time, end_time time.Time) []Candlestick
}
