package common

import (
	"math"
	"time"

	"github.com/montanaflynn/stats"
)

type Context struct {
	Currency            string
	Timeframe           string
	Time                time.Time
	CandlesticksGateway CandlesticksGateway
	EquityGateway       EquityGateway
	TradesGateway       TradesGateway
	Logger              Logger
}

func (context Context) Open(shift int) float64 {
	candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift))

	return candlestick.Open
}

func (context Context) High(shift int) float64 {
	candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift))

	return candlestick.High
}

func (context Context) Low(shift int) float64 {
	candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift))

	return candlestick.Low
}

func (context Context) Close(shift int) float64 {
	candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift))

	return candlestick.Close
}

func (context Context) SMA(period int, shift int) float64 {
	values := []float64{}

	for i := 0; i < period; i++ {
		if close := context.Close(i + shift); close != 0.0 {
			values = append(values, context.Close(i+shift))
		}
	}

	sma, _ := stats.Mean(values)

	return sma
}

func (context Context) TR(shift int) float64 {
	current_candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift))
	previous_candlestick := context.CandlesticksGateway.GetCandlestick(context.Currency, context.Timeframe, ShiftTime(context.Time, context.Timeframe, shift+1))

	values := []float64{current_candlestick.High - current_candlestick.Low}

	if previous_candlestick.Close != 0.0 {
		values = append(values, math.Abs(current_candlestick.High-previous_candlestick.Close))
		values = append(values, math.Abs(current_candlestick.Low-previous_candlestick.Close))
	}

	tr, _ := stats.Max(values)

	return tr
}

func (context Context) ATR(period int, shift int) float64 {
	values := []float64{}

	for i := 0; i < period; i++ {
		values = append(values, context.TR(i+shift))
	}

	atr, _ := stats.Mean(values)

	return atr
}
