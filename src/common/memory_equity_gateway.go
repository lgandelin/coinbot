package common

import (
	"time"
)

type MemoryEquityGateway struct {
	equity EquityCurve
}

func (gateway MemoryEquityGateway) GetCurrentEquity() float64 {
	if len(gateway.equity) > 0 {
		return gateway.equity[len(gateway.equity)-1].Value
	}

	return 0.0
}

func (gateway MemoryEquityGateway) GetEquityCurve() EquityCurve {
	return gateway.equity
}

func (gateway *MemoryEquityGateway) UpdateCurrentEquity(value float64, t time.Time) {
	equity := Equity{
		Value: value,
		Time:  t,
	}
	gateway.equity = append(gateway.equity, equity)
}
