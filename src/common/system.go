package common

type System struct {
	ID                   string
	Operation            string
	Timeframe            string
	Entry                func() bool
	Exit                 func() bool
	StopLoss             func() float64
	ChildTradesThreshold func() float64
	ChildTradesEntry     func() bool
}
