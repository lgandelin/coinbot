package common

import (
	"time"
)

type TradesGateway interface {
	OpenTrade(trade *Trade)
	UpdateTradeSetHasChild(trade *Trade)
	CloseTrade(trade *Trade, time time.Time, close_price float64)
	GetOpenedTrades() TradesList
	GetTrades() TradesList
}
