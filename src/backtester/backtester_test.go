package backtester

import (
	"testing"

	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
)

func System1(context Context) System {

	return System{
		ID:        "M30-BUY-CRSVR-3-6",
		Operation: "BUY",
		Timeframe: "M30",
		Entry: func() bool {
			sma3_0 := context.SMA(3, 0)
			sma6_0 := context.SMA(6, 0)
			sma3_1 := context.SMA(3, 1)
			sma6_1 := context.SMA(6, 1)

			return sma3_0 > sma6_0 && sma3_1 < sma6_1
		},
		Exit: func() bool {
			sma3_0 := context.SMA(3, 0)
			sma6_0 := context.SMA(6, 0)
			sma3_1 := context.SMA(3, 1)
			sma6_1 := context.SMA(6, 1)

			return sma3_0 < sma6_0 && sma3_1 > sma6_1
		},
		StopLoss: func() float64 {
			return 0.0
		},
		ChildTradesThreshold: func() float64 {
			return 0.0
		},
		ChildTradesEntry: func() bool {
			return true
		},
	}
}

func TestBacktester(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		CandlesticksGateway: NewMockCandlesticksGateway(),
		EquityGateway:       &MemoryEquityGateway{},
		TradesGateway:       &MemoryTradesGateway{},
		Logger:              nil,
	}
	backtester := Backtester{
		Context:   context,
		StartTime: ParseTime("2017-07-01 16:30:00"),
		EndTime:   ParseTime("2017-07-02 16:00:00"),
	}

	expected_trades := TradesList{
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-01 22:00:00"), OpenPrice: 0.01558902, CloseTime: ParseTime("2017-07-02 00:30:00"), ClosePrice: 0.01559000, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 03:30:00"), OpenPrice: 0.0156034, CloseTime: ParseTime("2017-07-02 10:30:00"), ClosePrice: 0.01584035, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 11:30:00"), OpenPrice: 0.0160346, CloseTime: ParseTime("2017-07-02 13:30:00"), ClosePrice: 0.01582483, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 15:00:00"), OpenPrice: 0.0159255, CloseTime: ParseTime("2017-07-02 16:00:00"), ClosePrice: 0.01624888, SystemID: "M30-BUY-CRSVR-3-6"},
	}

	assert.Equal(t, expected_trades, backtester.Backtest(System1))
}

func System2(context Context) System {

	return System{
		ID:        "M30-BUY-CRSVR-3-6",
		Operation: "BUY",
		Timeframe: "M30",
		Entry: func() bool {
			sma3_0 := context.SMA(3, 0)
			sma6_0 := context.SMA(6, 0)
			sma3_1 := context.SMA(3, 1)
			sma6_1 := context.SMA(6, 1)

			return sma3_0 > sma6_0 && sma3_1 < sma6_1
		},
		Exit: func() bool {
			sma3_0 := context.SMA(3, 0)
			sma6_0 := context.SMA(6, 0)
			sma3_1 := context.SMA(3, 1)
			sma6_1 := context.SMA(6, 1)

			return sma3_0 < sma6_0 && sma3_1 > sma6_1
		},
		StopLoss: func() float64 {
			return 0.0001
		},
		ChildTradesThreshold: func() float64 {
			return 0.0
		},
		ChildTradesEntry: func() bool {
			return true
		},
	}
}

func TestBacktesterWithStopLoss(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		CandlesticksGateway: NewMockCandlesticksGateway(),
		EquityGateway:       &MemoryEquityGateway{},
		TradesGateway:       &MemoryTradesGateway{},
	}
	backtester := Backtester{
		Context:   context,
		StartTime: ParseTime("2017-07-01 16:30:00"),
		EndTime:   ParseTime("2017-07-02 16:00:00"),
	}

	expected_trades := TradesList{
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-01 22:00:00"), OpenPrice: 0.01558902, CloseTime: ParseTime("2017-07-02 00:30:00"), ClosePrice: 0.01559000, StopLossPrice: 0.01548902, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 03:30:00"), OpenPrice: 0.0156034, CloseTime: ParseTime("2017-07-02 10:30:00"), ClosePrice: 0.01584035, StopLossPrice: 0.0155034, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 11:30:00"), OpenPrice: 0.0160346, CloseTime: ParseTime("2017-07-02 13:00:00"), ClosePrice: 0.0159346, StopLossPrice: 0.0159346, SystemID: "M30-BUY-CRSVR-3-6"},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 15:00:00"), OpenPrice: 0.0159255, CloseTime: ParseTime("2017-07-02 16:00:00"), ClosePrice: 0.01624888, StopLossPrice: 0.0158255, SystemID: "M30-BUY-CRSVR-3-6"},
	}

	assert.Equal(t, expected_trades, backtester.Backtest(System2))
}

func System3(context Context) System {

	return System{
		ID:        "M30-BUY-CRSVR-12-48",
		Operation: "BUY",
		Timeframe: "M30",
		Entry: func() bool {
			sma12_0 := context.SMA(12, 0)
			sma48_0 := context.SMA(48, 0)
			sma12_1 := context.SMA(12, 1)
			sma48_1 := context.SMA(48, 1)

			return sma12_0 > sma48_0 && sma12_1 < sma48_1
		},
		Exit: func() bool {
			close := context.Close(0)
			sma48_0 := context.SMA(48, 0)

			return close < sma48_0
		},
		StopLoss: func() float64 {
			return 0.0
		},
		ChildTradesThreshold: func() float64 {
			return 5 * context.ATR(24, 0)
		},
		ChildTradesEntry: func() bool {
			return true
		},
	}
}

func TestBacktesterWithChildTrade(t *testing.T) {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		CandlesticksGateway: NewMockCandlesticksGateway(),
		EquityGateway:       &MemoryEquityGateway{},
		TradesGateway:       &MemoryTradesGateway{},
	}
	backtester := Backtester{
		Context:   context,
		StartTime: ParseTime("2017-07-01 16:30:00"),
		EndTime:   ParseTime("2017-07-05 16:00:00"),
	}

	expected_trades := TradesList{
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-02 08:30:00"), OpenPrice: 0.01584123, CloseTime: ParseTime("2017-07-05 07:00:00"), ClosePrice: 0.02007447, StopLossPrice: 0.0, SystemID: "M30-BUY-CRSVR-12-48", HasChildTrade: true},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-03 03:30:00"), OpenPrice: 0.017201, CloseTime: ParseTime("2017-07-05 07:00:00"), ClosePrice: 0.02007447, StopLossPrice: 0.0, SystemID: "M30-BUY-CRSVR-12-48", HasChildTrade: true},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-04 02:00:00"), OpenPrice: 0.01898999, CloseTime: ParseTime("2017-07-05 07:00:00"), ClosePrice: 0.02007447, StopLossPrice: 0.0, SystemID: "M30-BUY-CRSVR-12-48", HasChildTrade: true},
		&Trade{Operation: "BUY", Currency: "BTC-LTC", OpenTime: ParseTime("2017-07-04 21:00:00"), OpenPrice: 0.02074999, CloseTime: ParseTime("2017-07-05 07:00:00"), ClosePrice: 0.02007447, StopLossPrice: 0.0, SystemID: "M30-BUY-CRSVR-12-48"},
	}

	assert.Equal(t, expected_trades, backtester.Backtest(System3))
}
