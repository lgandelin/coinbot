package backtester

import (
	"time"

	. "gitlab.com/lgandelin/common"
)

type Backtester struct {
	Context   Context
	StartTime time.Time
	EndTime   time.Time
}

func (backtester *Backtester) Backtest(system func(Context) System) TradesList {
	backtester.Context.Time = backtester.StartTime

	trades_manager := TradesManager{
		Context: backtester.Context,
	}

	for !trades_manager.Context.Time.After(backtester.EndTime) {
		trades_manager.ManageTrades(system)
		trades_manager.Context.Time = ShiftTime(trades_manager.Context.Time, trades_manager.Context.Timeframe, 1, true)
	}

	backtester.Context = trades_manager.Context

	//Close last trades
	backtester.Context.Time = ShiftTime(backtester.Context.Time, backtester.Context.Timeframe, 1)

	for _, trade := range backtester.Context.TradesGateway.GetOpenedTrades() {
		backtester.Context.TradesGateway.CloseTrade(trade, backtester.Context.Time, backtester.Context.Close(0))
	}

	return backtester.Context.TradesGateway.GetTrades()
}
