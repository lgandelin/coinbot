package live_trader

import (
	. "gitlab.com/lgandelin/common"
)

type LiveTrader struct {
	Context Context
}

func (live_trader *LiveTrader) Trade(system func(Context) System) {

	tradesManager := TradesManager{
		Context: live_trader.Context,
	}
	tradesManager.ManageTrades(system)
}
