package systems

import (
	. "gitlab.com/lgandelin/common"
)

func System1(context Context) System {

	return System{
		ID:        "M30-BUY-CRSVR-12-48",
		Operation: "BUY",
		Timeframe: "M30",
		Entry: func() bool {
			sma12_0 := context.SMA(12, 0)
			sma48_0 := context.SMA(48, 0)
			sma12_1 := context.SMA(12, 1)
			sma48_1 := context.SMA(48, 1)

			return sma12_0 > sma48_0 && sma12_1 <= sma48_1
		},
		Exit: func() bool {
			sma12_0 := context.SMA(12, 0)
			sma48_0 := context.SMA(48, 0)
			sma12_1 := context.SMA(12, 1)
			sma48_1 := context.SMA(48, 1)

			return sma12_0 < sma48_0 && sma12_1 >= sma48_1
		},
		StopLoss: func() float64 {
			return 2.5 * context.ATR(24, 0)
		},
		ChildTradesThreshold: func() float64 {
			return 2.5 * context.ATR(24, 0)
		},
		ChildTradesEntry: func() bool {
			close := context.Close(0)
			sma12_0 := context.SMA(12, 0)

			return close > sma12_0
		},
	}
}
