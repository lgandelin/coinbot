package poloniex_equity_gateway

import (
	//"strconv"
	//"strings"
	"time"

	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/csv_trades_gateway"
	"gitlab.com/lgandelin/poloniex"
)

type PoloniexTradesGateway struct {
	Client           *poloniex.Poloniex
	CSVTradesGateway *CSVTradesGateway
}

func NewPoloniexTradesGateway(poloniex_client *poloniex.Poloniex, csv_file string) *PoloniexTradesGateway {
	return &PoloniexTradesGateway{
		Client: poloniex_client,
		CSVTradesGateway: &CSVTradesGateway{
			File: csv_file,
		},
	}
}

func (gateway *PoloniexTradesGateway) OpenTrade(trade *Trade) {
	if trade.Operation == "BUY" {
		gateway.Client.Buy(poloniex.ConvertCurrencyToPoloniex(trade.Currency), trade.OpenPrice, trade.Position)
	} else if trade.Operation == "SELL" {
		gateway.Client.Sell(poloniex.ConvertCurrencyToPoloniex(trade.Currency), trade.OpenPrice, trade.Position)
	}
	gateway.CSVTradesGateway.OpenTrade(trade)
}

func (gateway *PoloniexTradesGateway) CloseTrade(trade *Trade, time time.Time, close_price float64) {
	if trade.Operation == "BUY" {
		gateway.Client.Sell(poloniex.ConvertCurrencyToPoloniex(trade.Currency), close_price, trade.Position*0.9975)
	} else if trade.Operation == "SELL" {
		gateway.Client.Buy(poloniex.ConvertCurrencyToPoloniex(trade.Currency), close_price, trade.Position*0.9975)
	}
	gateway.CSVTradesGateway.CloseTrade(trade, time, close_price)
}

func (gateway PoloniexTradesGateway) GetOpenedTrades() TradesList {
	/*trades := TradesList{}
	poloniex_trades, _ := gateway.Client.PrivateTradeHistoryAll()

	for currency, poloniex_history_entry := range poloniex_trades {
		poloniex_trade := poloniex_history_entry[0]

		trade := Trade{
			ID:        strconv.FormatInt(poloniex_trade.OrderNumber, 10),
			Operation: strings.ToUpper(poloniex_trade.Type),
			Currency:  currency,
			OpenPrice: poloniex_trade.Rate,
			OpenTime:  ParseTime(poloniex_trade.Date),
			Position:  poloniex_trade.Amount,
		}

		trades = append(trades, &trade)
	}*/

	trades := gateway.CSVTradesGateway.GetOpenedTrades()

	return trades
}

func (gateway PoloniexTradesGateway) GetTrades() TradesList {
	trades := TradesList{}

	return trades
}

func (gateway *PoloniexTradesGateway) UpdateTradeSetHasChild(trade *Trade) {
}
