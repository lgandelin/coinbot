package bolt_candlesticks_gateway

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"time"

	. "gitlab.com/lgandelin/common"
)

type BoltCandlesticksGateway struct {
	Database *bolt.DB
}

func NewBoltCandlesticksGateway() BoltCandlesticksGateway {
	db, err := bolt.Open("/data/candlesticks.db", 0777, nil)
	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close()

	return BoltCandlesticksGateway{
		Database: db,
	}
}

func (gateway BoltCandlesticksGateway) GetCandlestick(currency string, timeframe string, time time.Time) Candlestick {
	candlestick := Candlestick{}
	err := gateway.Database.View(func(tx *bolt.Tx) error {
		candlesticks_bucket := []byte(currency + "_" + timeframe)
		bucket := tx.Bucket(candlesticks_bucket)
		if bucket == nil {
			fmt.Println("Bucket %s not found!", candlesticks_bucket)

			return nil
		}

		key := currency + "#" + timeframe + "#" + time.Format("2006-01-02 15:04:05")
		candlestick_bytes := bucket.Get([]byte(key))

		//Gob Decode
		b2 := bytes.Buffer{}
		b2.Write(candlestick_bytes)
		d := gob.NewDecoder(&b2)
		d.Decode(&candlestick)

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return candlestick
}

func (gateway BoltCandlesticksGateway) GetCandlesticks(currency string, timeframe string, startTime time.Time, endTime time.Time) []Candlestick {
	candlesticks := []Candlestick{}

	time := startTime
	for (time.Equal(startTime) || time.After(startTime)) && (time.Equal(endTime) || time.Before(endTime)) {
		if candlestick := gateway.GetCandlestick(currency, timeframe, time); candlestick.Close != 0 {
			candlesticks = append(candlesticks, candlestick)
		}
		time = ShiftTime(time, timeframe, 1, true)
	}

	if len(candlesticks) == 0 {
		fmt.Println(startTime, endTime)
	}

	return candlesticks
}

func (gateway BoltCandlesticksGateway) InsertCandlestick(candlestick Candlestick) error {

	//Gob Encode
	key := candlestick.Currency + "#" + candlestick.Timeframe + "#" + candlestick.CloseTime.Format("2006-01-02 15:04:00")
	candlestick_bytes := bytes.Buffer{}
	e := gob.NewEncoder(&candlestick_bytes)
	err := e.Encode(candlestick)

	if err != nil {
		fmt.Println("Failed to gob encode", err)
	}

	//Store candlestick data
	err = gateway.Database.Update(func(tx *bolt.Tx) error {
		candlesticks_bucket := []byte(candlestick.Currency + "_" + candlestick.Timeframe)
		bucket, err := tx.CreateBucketIfNotExists(candlesticks_bucket)
		if err != nil {
			log.Fatal(err)
			return nil
		}

		err = bucket.Put([]byte(key), candlestick_bytes.Bytes())
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return nil
}
