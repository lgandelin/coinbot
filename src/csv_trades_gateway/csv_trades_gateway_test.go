package csv_trades_gateway

import (
	"github.com/stretchr/testify/assert"
	. "gitlab.com/lgandelin/common"
	"testing"
)

func TestCSVTradesGateway(t *testing.T) {
	csv_trades_gateway := CSVTradesGateway{
		File: "./trades.csv",
	}

	t1 := Trade{
		ID:            "38731518",
		Operation:     "BUY",
		Currency:      "BTC-LTC",
		OpenPrice:     0.01904,
		OpenTime:      ParseTime("2017-07-15 17:00:00"),
		ClosePrice:    0.01987,
		CloseTime:     ParseTime("2017-07-18 13:30:00"),
		StopLossPrice: 0.01886,
		Position:      0.5569,
		SystemID:      "MA-CSRV-12-48",
		HasChildTrade: false,
	}

	t2 := Trade{
		ID:            "38731529",
		Operation:     "SELL",
		Currency:      "BTC-ETH",
		OpenPrice:     0.084568,
		OpenTime:      ParseTime("2017-06-02 08:30:00"),
		StopLossPrice: 0.0826432,
		Position:      0.155,
		SystemID:      "MA-CSRV-12-48",
		HasChildTrade: false,
	}

	assert.Equal(t, TradesList{&t1, &t2}, csv_trades_gateway.GetTrades())
	assert.Equal(t, TradesList{&t2}, csv_trades_gateway.GetOpenedTrades())
}
