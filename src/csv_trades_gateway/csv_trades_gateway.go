package csv_trades_gateway

import (
	"bufio"
	. "gitlab.com/lgandelin/common"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

type CSVTradesGateway struct {
	File string
}

func (gateway CSVTradesGateway) GetTrades() TradesList {
	trades := TradesList{}

	file, err := os.Open(gateway.File)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()

		if line != "" {
			data := strings.Split(line, ",")

			id := data[0]
			operation := data[1]
			currency := data[2]
			open_price, _ := strconv.ParseFloat(data[3], 64)
			open_time := ParseTime(data[4])
			close_price, _ := strconv.ParseFloat(data[5], 64)
			close_time := ParseTime(data[6])
			stop_loss_price, _ := strconv.ParseFloat(data[7], 64)
			position, _ := strconv.ParseFloat(data[8], 64)
			system_id := data[9]
			has_child_trade, _ := strconv.ParseBool(data[10])

			trade := Trade{
				ID:            id,
				Operation:     operation,
				Currency:      currency,
				OpenPrice:     open_price,
				OpenTime:      open_time,
				ClosePrice:    close_price,
				CloseTime:     close_time,
				StopLossPrice: stop_loss_price,
				Position:      position,
				SystemID:      system_id,
				HasChildTrade: has_child_trade,
			}
			trades = append(trades, &trade)
		}
	}

	return trades
}

func (gateway *CSVTradesGateway) OpenTrade(trade *Trade) {
	trade.ID = generateTradeID()
	trades := gateway.GetTrades()
	trades = append(trades, trade)
	WriteTradesToCSV(trades, gateway.File, false)
}

func (gateway *CSVTradesGateway) CloseTrade(trade *Trade, time time.Time, close_price float64) {
	trades := gateway.GetTrades()
	for _, t := range trades {
		if t.ID == trade.ID {
			t.CloseTime = time
			t.ClosePrice = close_price
		}
	}
	WriteTradesToCSV(trades, gateway.File, false)
}

func (gateway CSVTradesGateway) GetOpenedTrades() TradesList {
	trades := TradesList{}

	for _, trade := range gateway.GetTrades() {
		if trade.IsOpened() {
			trades = append(trades, trade)
		}
	}

	return trades
}

func (gateway CSVTradesGateway) UpdateTradeSetHasChild(trade *Trade) {
	trades := gateway.GetTrades()
	for _, t := range trades {
		if t.ID == trade.ID {
			t.HasChildTrade = true
		}
	}
	WriteTradesToCSV(trades, gateway.File, false)
}

func WriteTradesToCSV(trades TradesList, filename string, append_to_file bool) {
	lines := []string{}
	for _, trade := range trades {
		line := tradeToCSVLine(trade)
		lines = append(lines, line)
	}

	writeToCSV(lines, filename, append_to_file)
}

func writeToCSV(lines []string, filename string, append_to_file bool) {
	var file *os.File
	var err error

	if append_to_file {
		file, err = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	} else {
		file, err = os.Create(filename)
	}

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	for _, line := range lines {
		file.WriteString(line + "\n")
	}
}

func tradeToCSVLine(trade *Trade) string {
	data := []string{
		trade.ID,
		trade.Operation,
		trade.Currency,
		strconv.FormatFloat(trade.OpenPrice, 'f', 8, 64),
		trade.OpenTime.Format("2006-01-02 15:04:05"),
		strconv.FormatFloat(trade.ClosePrice, 'f', 8, 64),
		trade.CloseTime.Format("2006-01-02 15:04:05"),
		strconv.FormatFloat(trade.StopLossPrice, 'f', 8, 64),
		strconv.FormatFloat(trade.Position, 'f', 8, 64),
		trade.SystemID,
		strconv.FormatBool(trade.HasChildTrade),
	}

	return strings.Join(data, ",")
}

func generateTradeID() string {
	return get_random_string(16)
}

func get_random_string(n int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
