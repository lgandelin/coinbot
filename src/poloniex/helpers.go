package poloniex

import (
	"strings"
)

func ConvertCurrencyFromPoloniex(currency string) string {
	return strings.Replace(currency, "_", "-", -1)
}

func ConvertCurrencyToPoloniex(currency string) string {
	return strings.Replace(currency, "-", "_", -1)
}

func ConvertTimeframeFromPoloniex(period int) string {
	timeframe := "D1"

	if period == 1800 {
		timeframe = "M30"
	}

	return timeframe
}

func ConvertTimeframeToPoloniex(timeframe string) int {
	period := 86400

	if timeframe == "M30" {
		period = 1800
	}

	return period
}
