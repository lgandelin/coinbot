package main

import (
	"fmt"

	. "gitlab.com/lgandelin/analyzer"
	. "gitlab.com/lgandelin/backtester"
	. "gitlab.com/lgandelin/bolt_candlesticks_gateway"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/csv_trades_gateway"
	. "gitlab.com/lgandelin/logrus_file_logger"
	. "gitlab.com/lgandelin/systems"
)

func main() {
	WriteTradesToCSV(TradesList{}, "/data/backtester_trades.csv", false)

	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		CandlesticksGateway: NewBoltCandlesticksGateway(),
		EquityGateway:       &MemoryEquityGateway{},
		TradesGateway: &CSVTradesGateway{
			File: "/data/backtester_trades.csv",
		},
		Logger: NewLogrusFileLogger("/data/logs/backtester.log"),
	}
	backtester := Backtester{
		Context:   context,
		StartTime: ParseTime("2016-07-15 00:00:00"),
		EndTime:   ParseTime("2017-07-15 00:00:00"),
	}
	trades := backtester.Backtest(System1)

	fmt.Println("Trades : ", len(trades))
	fmt.Println("Win % : ", WinPercentage(trades))
	fmt.Println("Total (R) : ", TotalRMultiple(trades))
	fmt.Println("Exp. (R) : ", ExpectancyInRMultiple(trades))
}
