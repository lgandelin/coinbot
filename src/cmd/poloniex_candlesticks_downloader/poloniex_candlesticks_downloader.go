package main

import (
	"fmt"
	"os"
	"time"

	"github.com/Sirupsen/logrus"
	. "gitlab.com/lgandelin/bolt_candlesticks_gateway"
	. "gitlab.com/lgandelin/common"
	"gitlab.com/lgandelin/poloniex"
)

func main() {
	//Setup log
	f, err := os.OpenFile("/data/logs/candlesticks.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		fmt.Printf("Error opening log file: %v", err)
	}

	defer f.Close()

	logrus.SetOutput(f)

	client := poloniex.New(os.Getenv("API_KEY"), os.Getenv("API_SECRET"))

	timeframe := "M30"

	poloniex_currencies := []string{"BTC_LTC", "BTC_ETH", "BTC_ETC"}
	poloniex_timeframe := 1800

	gateway := NewBoltCandlesticksGateway()

	for _, poloniex_currency := range poloniex_currencies {
		t := RoundTime(time.Now(), timeframe)
		candles, err := client.ChartData(poloniex_currency, poloniex_timeframe, ShiftTime(t, timeframe, 2*48*500), ShiftTime(t, timeframe, 1))
		if err != nil {
			panic(err)
		}

		currency := poloniex.ConvertCurrencyFromPoloniex(poloniex_currency)

		for _, candle := range candles {
			t := time.Unix(candle.Date, 0)
			close_time := ShiftTime(t, timeframe, 1)
			candlestick := Candlestick{
				Currency:  currency,
				CloseTime: close_time,
				Timeframe: timeframe,
				Open:      candle.Open,
				High:      candle.High,
				Low:       candle.Low,
				Close:     candle.Close,
			}
			if existing_candlestick := gateway.GetCandlestick(currency, timeframe, close_time); existing_candlestick.Close == 0 {
				logrus.WithFields(logrus.Fields{
					"candlestick": candlestick,
				}).Info("Inserted candlestick")

				gateway.InsertCandlestick(candlestick)
			}
		}
	}
}
