package main

import (
	"os"
	"time"

	. "gitlab.com/lgandelin/bolt_candlesticks_gateway"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/live_trader"
	"gitlab.com/lgandelin/poloniex"
	. "gitlab.com/lgandelin/poloniex_equity_gateway"
	. "gitlab.com/lgandelin/poloniex_trades_gateway"
	. "gitlab.com/lgandelin/systems"
)

func main() {
	poloniex_client := poloniex.New(os.Getenv("POLONIEX_API_KEY"), os.Getenv("POLONIEX_API_SECRET"))

	context := Context{
		Currency:            "BTC-ETC",
		Timeframe:           "M30",
		CandlesticksGateway: NewBoltCandlesticksGateway(),
		Time:                RoundTime(time.Now(), "M30"),
		TradesGateway:       NewPoloniexTradesGateway(poloniex_client, "/data/live_trades.csv"),
		EquityGateway:       NewPoloniexEquityGateway(poloniex_client),
	}

	live_trader := LiveTrader{
		Context: context,
	}

	live_trader.Trade(System1)
}
