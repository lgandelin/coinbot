package main

import (
	"fmt"

	"gitlab.com/lgandelin/analyzer"
	. "gitlab.com/lgandelin/bolt_candlesticks_gateway"
	. "gitlab.com/lgandelin/common"
	. "gitlab.com/lgandelin/csv_trades_gateway"
	"gitlab.com/lgandelin/monte_carlo_simulator"
)

func main() {
	context := Context{
		Currency:            "BTC-LTC",
		Timeframe:           "M30",
		CandlesticksGateway: NewBoltCandlesticksGateway(),
		TradesGateway: &CSVTradesGateway{
			File: "/data/backtester_trades.csv",
		},
	}
	trades := context.TradesGateway.GetTrades()

	fmt.Println("Trades : ", len(trades))
	fmt.Println("Win % : ", analyzer.WinPercentage(trades))
	fmt.Println("Total (R) : ", analyzer.TotalRMultiple(trades))
	fmt.Println("Exp. (R) : ", analyzer.ExpectancyInRMultiple(trades))

	simulations := 2500
	years := 1.0
	starting_equity := 0.2

	trades_distributions := monte_carlo_simulator.GenerateTradesDistributions(trades, simulations, years)
	equity_curves := GenerateEquityCurves(context, trades_distributions, starting_equity)

	analyzer.PrintMonteCarloReport(trades_distributions, equity_curves, years)
}
