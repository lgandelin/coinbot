package logrus_logger

import (
	"fmt"
	"os"

	"github.com/Sirupsen/logrus"
	. "gitlab.com/lgandelin/common"
)

type LogrusFileLogger struct {
	LogrusLogger *logrus.Logger
}

func NewLogrusFileLogger(log_file string) *LogrusFileLogger {
	f, err := os.OpenFile(log_file, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)

	if err != nil {
		fmt.Printf("Error opening log file: %v", err)
	}

	logger := logrus.New()
	logger.Out = f

	return &LogrusFileLogger{
		LogrusLogger: logger,
	}
}

func (logger *LogrusFileLogger) Info(message string, fields LoggerFields) {
	logger.LogrusLogger.WithFields(logrus.Fields(fields)).Info(message)
}
