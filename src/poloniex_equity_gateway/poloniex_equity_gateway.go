package poloniex_equity_gateway

import (
	"time"

	. "gitlab.com/lgandelin/common"
	"gitlab.com/lgandelin/poloniex"
)

type PoloniexEquityGateway struct {
	Client *poloniex.Poloniex
}

func NewPoloniexEquityGateway(poloniex_client *poloniex.Poloniex) *PoloniexEquityGateway {
	return &PoloniexEquityGateway{
		Client: poloniex_client,
	}
}

func (gateway PoloniexEquityGateway) GetCurrentEquity() float64 {
	balances, _ := gateway.Client.Balances()

	balance := 0.0

	for _, b := range balances {
		balance += b.Available
	}

	return balance
}

func (gateway PoloniexEquityGateway) GetEquityCurve() EquityCurve {
	return EquityCurve{}
}

func (gateway PoloniexEquityGateway) UpdateCurrentEquity(value float64, t time.Time) {

}
