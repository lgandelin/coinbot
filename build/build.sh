IMAGE=$1
COMPONENT=$2

docker exec bitcoinbot_go_1 sh -c "cd /go/src/gitlab.com/lgandelin/cmd/$COMPONENT && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main $COMPONENT.go"
cp /home/ubuntu/Code/bitcoin-bot/src/cmd/$COMPONENT/main /home/ubuntu/Code/bitcoin-bot/build/files
cd /home/ubuntu/Code/bitcoin-bot/build/files
docker build -t lgandelin/$IMAGE . && docker push lgandelin/$IMAGE
rm -f /home/ubuntu/Code/bitcoin-bot/build/files/main